import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {UsersModule} from '../user/users.module';
import { LoginComponent } from './components/login/login.component';
import {LoginRoutingModule} from './login-routing.module';
import {MatButtonModule, MatCardModule, MatInputModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { SignUpComponent } from './components/signup/sign-up.component';
import {ShareModule} from '../share/share.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WelcomeComponent,
    SignUpComponent,
  ],
  imports: [
    BrowserModule,
    UsersModule,
    LoginRoutingModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    ShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
