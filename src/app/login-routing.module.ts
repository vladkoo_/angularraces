import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {NgModule} from '@angular/core';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {SignUpComponent} from './components/signup/sign-up.component';
import {EnterGuard} from './enter.guard';

const routes: Routes = [
  {
    path: '', canActivateChild: [EnterGuard], children: [
      {path: '', component: WelcomeComponent},
      {path: 'login', component: LoginComponent},
      {path: 'signup', component: SignUpComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {
}
