import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../share/services/auth.service';
import {ShareService} from '../../../share/services/share.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  preload: boolean;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private shareService: ShareService) {
    this.loginForm = this.fb.group({
      login: ['', [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/)]],
      password: ['', [Validators.required, Validators.pattern(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)]],
    });
  }

  getLoginErrorMessage() {
    return this.loginForm.get('login').hasError('required') ? 'You must enter a value' :
      this.loginForm.get('login').hasError('pattern') ? 'Not a valid login' :
        '';
  }

  getPasswordErrorMessage() {
    return this.loginForm.get('password').hasError('required') ? 'You must enter a value' :
      this.loginForm.get('password').hasError('pattern') ? 'Not a valid password' :
        '';
  }

  logIn() {
    this.authService.login(this.loginForm.value)
      .subscribe(success => {
        if (success) {
          alert('You logged successfully');
          this.router.navigate(['/users']);
        } else {
          alert('Error, please check the fields');
        }
      });
  }

  ngOnInit() {
    this.shareService.dataSubj$.subscribe(flag => {
      this.preload = flag;
    });
  }

}
