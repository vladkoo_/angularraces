import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {ConfirmPasswordValidator} from './сonfrim-password.validator';
import {AuthService} from '../../../share/services/auth.service';
import {Router} from '@angular/router';
import {ShareService} from '../../../share/services/share.service';


function confirmPassword(password: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    console.log(`Validator value is ${password}`);
    if (control.value === undefined) {
      return {required: true};
    }
    if (control.value !== password) {
      return {confirm: true};
    }
    return null;
  };
}


@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  SignUpForm: FormGroup;
  checkPassword: string;
  preload: boolean;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private shareService: ShareService) {
    this.SignUpForm = this.fb.group({
      login: ['', [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/)]],
      password: ['', [Validators.required, Validators.pattern(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)]],
      // repPassword: ['', [confirmPassword(this.checkPassword)]],
      repPassword: [''],
    }, {validator: ConfirmPasswordValidator.MatchPassword});
  }

  getLoginErrorMessage() {
    return this.SignUpForm.get('login').hasError('required') ? 'You must enter a value' :
      this.SignUpForm.get('login').hasError('pattern') ? 'Not a valid login' :
        '';
  }

  getPasswordErrorMessage() {
    return this.SignUpForm.get('password').hasError('required') ? 'You must enter a value' :
      this.SignUpForm.get('password').hasError('pattern') ? 'Not a valid password' :
        '';
  }

  getRepPasswordErrorMessage() {
    return this.SignUpForm.get('repPassword').hasError('ConfirmPassword') ? 'Passsword and ConfirmPassword didn\'t match' :
      this.SignUpForm.get('repPassword').hasError('required') ? 'You must enter a value' : '';
  }

  // getRepPasswordErrorMessage() {
  //   return this.SignUpForm.get('repPassword').hasError('required') ? 'Nothing' :
  //     this.SignUpForm.get('repPassword').hasError('confirm') ? 'Not match' :
  //       '';
  // }

  changePas() {
    if (this.SignUpForm.get('password').value.toString() === this.SignUpForm.get('repPassword').value.toString()) {
      console.log('done');
      this.SignUpForm.get('repPassword').setValue(this.checkPassword);
    } else {
      console.log('no');
      this.SignUpForm.get('repPassword').setErrors({confirm: true});
    }
  }

  comparePasswords() {
    console.log(this.SignUpForm.get('password').value.toString());
    this.checkPassword = this.SignUpForm.get('password').value.toString();
    this.SignUpForm.get('repPassword').setValidators(confirmPassword(this.checkPassword));
  }

  signUp() {
    const customer = this.SignUpForm.value;
    delete customer.repPassword;
    this.authService.signUp(customer).subscribe(success => {
      if (success) {
        alert('You successfully created an account');
        this.router.navigate(['/login']);
      } else {
        alert('There is a customer with this login. Please choose another');
      }
    });
  }

  ngOnInit() {
    this.shareService.dataSubj$.subscribe(flag => {
      this.preload = flag;
    });
  }

}
