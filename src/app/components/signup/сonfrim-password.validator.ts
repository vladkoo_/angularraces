import {AbstractControl} from '@angular/forms';

export class ConfirmPasswordValidator {
  static MatchPassword(control: AbstractControl) {
    const password = control.get('password').value;

    const confirmPassword = control.get('repPassword').value.toString();
    if (!confirmPassword) {
      return control.get('repPassword').setErrors( {required: true} );
    }

    if (password !== confirmPassword) {
      return control.get('repPassword').setErrors( {ConfirmPassword: true} );
    } else {
      return null;
    }
  }
}
