import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {FormBuilder} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {UserInterface} from '../../intefaces/user.inteface';
import {EditUserInterface} from '../../intefaces/edit-user.interface';
import {ShareService} from '../../../share/services/share.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  _id: string;
  user: EditUserInterface;
  userForm: FormGroup;
  preload: boolean;

  constructor(private route: ActivatedRoute,
              private _location: Location,
              private fb: FormBuilder,
              private userService: UserService,
              private shareService: ShareService) {
    this._id = route.snapshot.params.id;
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern(/^[а-яА-ЯёЁa-zA-Z]+$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^[а-яА-ЯёЁa-zA-Z]+$/)]],
      age: [null, [Validators.required, Validators.pattern(/(^[1-9][0-9]?$)|(^100$)/)]],
      email: ['', [Validators.required, Validators.email]],
    });
    this.user = {
      firstName: '',
      lastName: '',
      age: null,
      email: '',
    };
  }

  getFirstNameErrorMessage() {
    return this.userForm.get('firstName').hasError('required') ? 'You must enter a value' :
      this.userForm.get('firstName').hasError('pattern') ? 'Not a valid first name' :
        '';
  }

  getLastNameErrorMessage() {
    return this.userForm.get('lastName').hasError('required') ? 'You must enter a value' :
      this.userForm.get('lastName').hasError('pattern') ? 'Not a valid last name' :
        '';
  }

  getAgeErrorMessage() {
    return this.userForm.get('age').hasError('required') ? 'You must enter a value' :
      this.userForm.get('age').hasError('pattern') ? 'Not a valid age' :
        '';
  }

  getEmailErrorMessage() {
    return this.userForm.get('email').hasError('required') ? 'You must enter a value' :
      this.userForm.get('email').hasError('pattern') ? 'Not a valid email' :
        '';
  }

  editUser() {
    this.userService.editUser(this._id, this.userForm.value).subscribe((data) => {
      console.log(data);
      this._location.back();
    }, (error) => {
      console.log(error);
    });
  }

  addUser() {
    this.userService.addUser(this.userForm.value).subscribe((data) => {
      console.log(data);
      this._location.back();
    });
  }

  ngOnInit() {
    this.shareService.dataSubj$.subscribe(flag => {
      this.preload = flag;
    });
    if (this._id) {
      this.userService.getOneUser(this._id).subscribe((data: UserInterface) => {
        console.log(data);
        Object.keys(this.userForm.getRawValue()).forEach(key => {
          this.user[key] = data[key];
        });
        this.userForm.setValue(this.user);
      });
    }
  }

  // fillForm() {
  //   this.userForm = this.fb.group({
  //     firstName: [this.user.firstName, [Validators.required, Validators.pattern(/^[а-яА-ЯёЁa-zA-Z]+$/)]],
  //     lastName: [this.user.lastName, [Validators.required, Validators.pattern(/^[а-яА-ЯёЁa-zA-Z]+$/)]],
  //     age: [this.user.age, [Validators.required, Validators.pattern(/(^[1-9][0-9]?$)|(^100$)/)]],
  //     email: [this.user.email, [Validators.required, Validators.email]],
  //   });
  // }

}
