import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTable} from '@angular/material';
import {UserService} from '../../services/user.service';
import {UserInterface} from '../../intefaces/user.inteface';
import {AuthService} from '../../../share/services/auth.service';
import {Router} from '@angular/router';
import {ShareService} from '../../../share/services/share.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  @ViewChild('usersTable') usersTable: MatTable<UserInterface>;

  displayedColumns: string[] = ['firstName', 'lastName', 'age', 'email', 'editButton', 'deleteButton'];
  dataSource: UserInterface[];
  preload: boolean;

  constructor(private userService: UserService, private authService: AuthService, private router: Router,
              private shareService: ShareService) {
  }

  logOut() {
    this.authService.logout().subscribe(success => {
      if (success) {
        alert('Logout is successful');
        this.router.navigate(['../']);
      } else {
        alert('Logout is failed');
      }
    });
  }

  deleteUser(element) {
    this.userService.deleteUser(element._id).subscribe((data) => {
      console.log(data);
      this.userService.getListOfUsers().subscribe((userList: UserInterface[]) => {
        this.dataSource = userList;
      });
      this.usersTable.renderRows();
    });
  }

  ngOnInit() {
    this.shareService.dataSubj$.subscribe(flag => {
      this.preload = flag;
    });
    this.userService.getListOfUsers().subscribe((data: UserInterface[]) => {
      this.dataSource = data;
      console.log(data);
    });
  }

}
