import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EditUserInterface} from '../intefaces/edit-user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getListOfUsers() {
    return this.http.get('http://localhost:3000/list');
  }

  getOneUser(id: string) {
    return this.http.get('http://localhost:3000/users/' + id);
  }

  editUser(id: string, user: EditUserInterface) {
    const transferObject = Object.assign({id, ...user});
    return this.http.put('http://localhost:3000/users', transferObject);
  }

  addUser(user: EditUserInterface) {
    return this.http.post('http://localhost:3000/users', user);
  }

  deleteUser(id: string) {
    return this.http.delete('http://localhost:3000/users/' + id);
  }
}
