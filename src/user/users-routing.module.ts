import {RouterModule, Routes} from '@angular/router';

import {EditUserComponent} from './components/edit-user/edit-user.component';

import {NgModule} from '@angular/core';
import {UsersListComponent} from './components/users-list/users-list.component';
import {AuthGuard} from './services/Guards/auth.guard';

const routes: Routes = [
  {
    path: 'users', canActivateChild: [AuthGuard], children: [
      {path: '', component: UsersListComponent},
      {path: 'new', component: EditUserComponent},
      {path: 'edit/:id', component: EditUserComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
