export interface EditUserInterface {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
}
