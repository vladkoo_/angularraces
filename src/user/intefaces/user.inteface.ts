export interface UserInterface {
  _id: string;
  firstName: string;
  lastName: string;
  age: number;
  email: string;
}
