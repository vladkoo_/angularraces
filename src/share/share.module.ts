import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthService} from './services/auth.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {MatProgressSpinnerModule} from '@angular/material';
import {CirclePreloaderComponent} from './components/preloader/circlePreloader.component';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {PreloaderInterceptor} from './interceptors/preloader.interceptor';
import {QueryPreloaderComponent} from './components/query-preloader/query-preloader.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';

@NgModule({
  declarations: [CirclePreloaderComponent, QueryPreloaderComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: PreloaderInterceptor,
      multi: true
    },
  ],
  exports: [MatProgressSpinnerModule,
    CirclePreloaderComponent,
    MatProgressBarModule,
    QueryPreloaderComponent
  ]
})
export class ShareModule {
}
