import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CirclePreloaderComponent } from './circlePreloader.component';

describe('CirclePreloaderComponent', () => {
  let component: CirclePreloaderComponent;
  let fixture: ComponentFixture<CirclePreloaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CirclePreloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CirclePreloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
