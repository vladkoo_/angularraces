import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CustomerInterface} from '../../app/interfaces/customer.interface';
import {tap} from 'rxjs/internal/operators/tap';
import {catchError, mapTo} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';
import {TokensInterface} from '../interfaces/tokens.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';

  constructor(private http: HttpClient) {
  }

  signUp(customer: CustomerInterface) {
    return this.http.post('http://localhost:3000/customers/signup', customer)
      .pipe(
        mapTo(true),
        catchError(error => {
          console.log(error);
          return of(false);
        })
      );
  }

  login(customer: CustomerInterface) {
    return this.http.post('http://localhost:3000/customers/login', customer)
      .pipe(
        tap((tokens: TokensInterface) => this.storeTokens(tokens)),
        mapTo(true),
        catchError(error => {
          console.log(error);
          return of(false);
        })
      );
  }

  logout() {
    return this.http.post('http://localhost:3000/customers/logout', {
      refreshToken: this.getRefreshToken()
    }).pipe(
      tap(() => this.removeTokens()),
      mapTo(true),
      catchError(error => {
        alert(error.error);
        return of(false);
      })
    );
  }

  isLoggedIn() {
    return this.getJwtToken();
  }

  refreshToken() {
    return this.http.post('http://localhost:3000/customers/refresh', {
      refreshToken: this.getRefreshToken(),
    });
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  storeTokens(tokens: TokensInterface) {
    console.log('store');
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
  }

  removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }
}
