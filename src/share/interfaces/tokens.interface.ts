export interface TokensInterface {
  jwt: string;
  refreshToken: string;
}
