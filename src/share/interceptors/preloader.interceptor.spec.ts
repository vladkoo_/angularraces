import { TestBed } from '@angular/core/testing';

import { PreloaderInterceptor } from './preloader.interceptor';

describe('PreloaderInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreloaderInterceptor = TestBed.get(PreloaderInterceptor);
    expect(service).toBeTruthy();
  });
});
